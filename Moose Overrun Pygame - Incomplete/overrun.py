import pygame, sys
import random
import math
from pygame.locals import *


# INITIALIZATION
pygame.init()
fps = pygame.time.Clock()

screen_rect = ( 1280, 720 )
window = pygame.display.set_mode( screen_rect )
pygame.display.set_caption( "Moose Overrun!" )




# CLASS DECLARATIONS
class GameObject:
    def __init__( self, image ):
        self.x = 1280/2
        self.y = 720/2
        self.width = 40
        self.height = 40
        self.image = image
        self.type_name = ""

    def Update( self ):
        a = 2

    def Draw( self, window ):
        window.blit( self.image, ( self.x, self.y ) )


class Player( GameObject ):
    def __init__( self, image ):
        GameObject.__init__( self, image )
        self.x = 350
        self.y = 350
        self.animate_speed = 0.1
        self.current_frame = 0
        self.max_frame = 4
        self.current_direction = 0
        self.speed = 5
        self.score = 0
        self.type_name = "player"

    def Update( self ):
        keys = pygame.key.get_pressed()
        is_moving = False

        if ( keys[K_UP] ):
            self.y -= self.speed
            is_moving = True
            self.current_direction = 2

        elif ( keys[K_DOWN] ):
            self.y += self.speed
            is_moving = True
            self.current_direction = 0

        elif ( keys[K_LEFT] ):
            self.x -= self.speed
            is_moving = True
            self.current_direction = 1

        elif ( keys[K_RIGHT] ):
            self.x += self.speed
            is_moving = True
            self.current_direction = 3

        if ( is_moving ):
            self.current_frame += self.animate_speed
            if ( self.current_frame >= self.max_frame ):
                self.current_frame = 0

    def Draw( self, window ):
        frame_rect = (
            int( self.current_frame ) * self.width,
            self.current_direction * self.height,
            self.width,
            self.height
        )

        window.blit( self.image, ( self.x, self.y ), frame_rect )


class Enemy( GameObject ):
    def __init__( self, image ):
        GameObject.__init__( self, image )
        self.choice_cooldown = 0
        self.current_choice = 0
        self.goal_x = -1
        self.goal_y = -1
        self.speed = 2
        self.type_name = "enemy"

    def LookForPlayer( self, objects ):

        self.goal_x = objects["player"].x
        self.goal_y = objects["player"].y

        nearest_left = None
        nearest_right = None
        nearest_top = None
        nearest_bottom = None

        nearest_left_x = 0
        nearest_right_x = 1280
        nearest_top_y = 0
        nearest_bottom_y = 720
        for key, obj in objects.items():
            # Same x? (same horizontal, different vertical?)
            

            # Look left
            # Look right
            # Look up
            # Look down


    def Update( self ):
        # Make a new choice?
        if ( self.goal_x > 0 ):
            if ( self.x < self.goal_x - 5 ):
                self.x += self.speed
            elif ( self.x > self.goal_x + 5 ):
                self.x -= self.speed

            if ( self.y < self.goal_y - 5 ):
                self.y += self.speed
            elif ( self.y > self.goal_y + 5 ):
                self.y -= self.speed

        elif ( self.choice_cooldown == 0 ):
            self.current_choice = random.randint( 1, 4 )
            self.choice_cooldown = random.randint( 10, 30 )

        # Move and update cooldown
        else:
            if ( self.current_choice == 1 ):
                self.x -= 1                           # move left
            elif ( self.current_choice == 2 ):
                self.x += 1                           # move right
            elif ( self.current_choice == 3 ):
                self.y -= 1                           # move up
            elif ( self.current_choice == 4 ):
                self.y += 1                           # move down

            self.choice_cooldown -= 1

    def Draw( self, window ):
        window.blit( self.image, ( self.x, self.y ) )



# HELPER FUNCTIONS
def Distance( obj1, obj2 ):
    diffx = obj2.x - obj1.x
    diffy = obj2.y - obj1.y
    return math.sqrt( diffx * diffx + diffy * diffy )

def SpawnEnemy( objects, image, spawn_pos, enemy_counter ):
    name = "enemy-" + str(enemy_counter)
    objects[ name ] = Enemy( image )
    objects[ name ].x = spawn_pos["x"]
    objects[ name ].y = spawn_pos["y"]

def SetupInanimateObjects( objects, images ):
    objects["table"] = GameObject( images["table"] )
    objects["table"].width = 207
    objects["table"].height = 158
    objects["table"].x = 1280/2 - objects["table"].width/2
    objects["table"].y = 720/2 - objects["table"].height/2
    objects["table"].type_name = "table"

    objects["trash"] = GameObject( images["trash"] )
    objects["trash"].x = 1000
    objects["trash"].y = 720/2 - objects["trash"].height/2
    objects["trash"].type_name = "trash"


# GAME VARIABLES
enemy_counter = 0
spawn_cooldown = 0
spawn_pos = { "x" : 1000, "y" : 720/2 }

# LOAD ASSETS
images = {
    "background" : pygame.image.load( "assets/graphics/background.png" ),
    "cat-sheet"  : pygame.image.load( "assets/graphics/cat-sheet.png" ),
    "moose"      : pygame.image.load( "assets/graphics/moose.png" ),
    "table"      : pygame.image.load( "assets/graphics/table.png" ),
    "trash"      : pygame.image.load( "assets/graphics/trash.png" )
}

colors = {
    "white" : pygame.Color( 255, 255, 255 ),
    "black" : pygame.Color( 0, 0, 0 )
}

font = pygame.font.Font( "assets/fonts/Moosomnia.ttf", 30 )
score_text = font.render( "Score: 0", False, colors["white"] )




# GAME OBJECTS
objects = {}
objects["player"] = Player( images["cat-sheet"] )
SetupInanimateObjects( objects, images )


# GAME LOOP
game_running = True
while ( game_running ):

    # GET INPUT
    for event in pygame.event.get():
        if ( event.type == QUIT ):          # window "quit" button pressed?
            pygame.quit()
            sys.exit()



    # UPDATE STATE
    if ( spawn_cooldown == 0 ):
        SpawnEnemy( objects, images["moose"], spawn_pos, enemy_counter )
        spawn_cooldown = 100
        objects["player"].score += 1
        score_text = font.render( "Score: " + str( objects["player"].score ), False, colors["white"] )
        enemy_counter += 1

    else:
        spawn_cooldown -= 1

    for key, obj in objects.items():
        obj.Update()


        if ( obj.type_name == "enemy" ):
            # Check collision
            if ( Distance( objects["player"], obj ) <= 25 ):
                objects["player"].x = 0
                objects["player"].y = 0

            # Check line of sight
            obj.LookForPlayer( objects )



    # DRAW
    window.fill( colors["black"] )          # fill screen with black

    window.blit( images["background"], ( 0, 0 ) )

    window.blit( score_text, ( 10, 10 ) )   # draw score text

    for key, obj in objects.items():
        obj.Draw( window )

    pygame.display.update()                 # refresh the screen
    fps.tick( 60 )
